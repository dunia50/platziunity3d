﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class PlayerController : MonoBehaviour
{

    float horizontalAxis;
    float verticalAxis;

    Vector3 mMovement;
    Animator mAnimator;

    public float turnSpeed = 20;
    Quaternion m_Rotation = Quaternion.identity;

    Rigidbody m_Rigidbody;


    // Start is called before the first frame update
    void Start()
    {
        mAnimator = GetComponent<Animator>();
        m_Rigidbody = GetComponent<Rigidbody>();
    }

    // Update is called once per frame
    void FixedUpdate()
    {
        horizontalAxis = Input.GetAxis("Horizontal");
        verticalAxis = Input.GetAxis("Vertical");

        mMovement.Set(horizontalAxis, 0f, verticalAxis);
        mMovement.Normalize();

        bool hasHorizontalInput = !Mathf.Approximately(horizontalAxis, 0f);
        bool hasVerticalInput = !Mathf.Approximately(verticalAxis, 0f);
        bool isWalking = hasHorizontalInput || hasVerticalInput;

        Debug.Log(isWalking);

        mAnimator.SetBool("isWalking", isWalking);

        Vector3 desiredForward = Vector3.RotateTowards(transform.forward, mMovement, turnSpeed * Time.deltaTime, 0f);
        m_Rotation = Quaternion.LookRotation(desiredForward);
    }

    void OnAnimatorMove()
    {
        m_Rigidbody.MovePosition(m_Rigidbody.position + mMovement * mAnimator.deltaPosition.magnitude);
        m_Rigidbody.MoveRotation(m_Rotation);
    }
}
